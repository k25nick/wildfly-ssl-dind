FROM k25nick/openjdk-photon:11.0.3
LABEL maintainer="k25nick@gmail.com"

ENV WILDFLY_VERSION 16.0.0.Final
ENV KEYCLOAK_VERSION 6.0.1
ENV JBOSS_HOME /opt/jboss/wildfly

USER root
COPY start.sh /start.sh
WORKDIR /opt/jboss/wildfly

RUN mkdir -p $JBOSS_HOME \
    && mkdir -p /opt/jboss \
    && chmod 755 /opt/jboss \
    && cd $JBOSS_HOME \
    && curl -O https://download.jboss.org/wildfly/$WILDFLY_VERSION/wildfly-$WILDFLY_VERSION.tar.gz \
	&& yum install -y gzip tar \
    && tar xf wildfly-$WILDFLY_VERSION.tar.gz --strip 1 \
    && curl -L https://downloads.jboss.org/keycloak/$KEYCLOAK_VERSION/adapters/keycloak-oidc/keycloak-wildfly-adapter-dist-$KEYCLOAK_VERSION.tar.gz | tar zx \
    && curl -L https://downloads.jboss.org/keycloak/$KEYCLOAK_VERSION/adapters/saml/keycloak-saml-wildfly-adapter-dist-$KEYCLOAK_VERSION.tar.gz | tar zx \
    && curl -fsSLO https://download.docker.com/linux/static/stable/x86_64/docker-18.09.0.tgz \
    && tar --strip-components=1 -xvzf docker-18.09.0.tgz -C /usr/local/bin docker/docker \
    && rm docker-18.09.0.tgz \
	&& yum remove -y gzip tar \
	&& yum clean all \
    && rm wildfly-$WILDFLY_VERSION.tar.gz \
    && rm -rf $JBOSS_HOME/bin/client \
    && rm -rf $JBOSS_HOME/domain \
    && rm -rf $JBOSS_HOME/appclient \
    && rm -rf $JBOSS_HOME/welcome-content/* \
    && rm -rf $JBOSS_HOME/docs \
    && chmod -R g+rw ${JBOSS_HOME} \
    && chmod +x /start.sh

ENV LAUNCH_JBOSS_IN_BACKGROUND true

EXPOSE 8080 8443

CMD ["/start.sh"]
