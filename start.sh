#!/bin/sh

openssl pkcs12 -export -name lv -in /keys/tls.crt -inkey /keys/tls.key -out /opt/jboss/wildfly/standalone/configuration/application.keystore -passout pass:pass123 -noiter -nomaciter
keytool -importcert -file /keys/tls.crt -alias lv -keystore /opt/jboss/wildfly/standalone/configuration/application.truststore -storepass pass123 -noprompt

/opt/jboss/wildfly/bin/standalone.sh -b 0.0.0.0
